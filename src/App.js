import "./App.css"
import { Header } from "./components/Header"
import { Todo } from "./components/Todo"

export const App = () => {
  return (
    <div className="app">
      <Header />
      <Todo />
    </div>
  )
}
