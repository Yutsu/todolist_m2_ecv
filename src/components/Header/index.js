import Navbar from "react-bootstrap/Navbar"
import "./index.css"

export const Header = () => {
  return (
    <Navbar bg="dark" variant="dark" className="containerHeader">
      <Navbar.Brand href="#home">Cours React</Navbar.Brand>
    </Navbar>
  )
}
