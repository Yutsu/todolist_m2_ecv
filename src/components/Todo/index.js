import { TodoList } from "./List"
import { TodoForm } from "./Form"
import { useState } from "react"
import { SwitchTheme } from "../SwitchTheme"
import "./index.css"

export const Todo = () => {
  const [todo, setTodo] = useState("")
  const [todoList, setTodoList] = useState([])
  const [isDarkTheme, setIsDarkTheme] = useState("false")

  return (
    <div className={isDarkTheme ? "containerDark" : "containerLight"}>
      <SwitchTheme setIsDarkTheme={setIsDarkTheme} isDarkTheme={isDarkTheme} />
      <TodoForm
        todo={todo}
        setTodo={setTodo}
        todoList={todoList}
        setTodoList={setTodoList}
      />
      <TodoList
        setTodoList={setTodoList}
        todoList={todoList}
        isDarkTheme={isDarkTheme}
      />
    </div>
  )
}
