import Button from "react-bootstrap/Button"
import "./index.css"

export const TodoListItem = ({ todoItem, todoList, setTodoList }) => {
  const deleteTodoItem = () => {
    setTodoList(todoList.filter((item) => item.id !== todoItem.id))
  }

  return (
    <div className="containerItem">
      <h5>{todoItem.name}</h5>
      <Button onClick={deleteTodoItem} variant="danger">
        Done
      </Button>
    </div>
  )
}
