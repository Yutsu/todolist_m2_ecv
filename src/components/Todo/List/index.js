import { TodoListItem } from "../Item"
import "./index.css"

export const TodoList = ({ todoList, setTodoList, isDarkTheme }) => {
  return todoList.map((todoItem) => (
    <div className={isDarkTheme ? "containerListDark" : "containerListLight"}>
      <TodoListItem
        todoList={todoList}
        setTodoList={setTodoList}
        todoItem={todoItem}
        key={todoItem.id}
        name={todoItem.name}
      />
    </div>
  ))
}
