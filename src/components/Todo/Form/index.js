import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import { nanoid } from "nanoid"
import "./index.css"

export const TodoForm = ({ todo, setTodo, todoList, setTodoList }) => {
  const handleChange = (event) => {
    setTodo(event.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    setTodoList([...todoList, { name: todo, id: nanoid() }])
    setTodo("")
  }

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group className="containerForm">
        <Form.Control value={todo} onChange={handleChange} type="text" />
        <Button variant="primary" type="submit">
          Add
        </Button>
      </Form.Group>
    </Form>
  )
}
