import Switch from "react-switch"
import { Moon, Sun } from "react-feather"
import "./index.css"

export const SwitchTheme = ({ isDarkTheme, setIsDarkTheme }) => {
  const handleChange = () => {
    setIsDarkTheme(!isDarkTheme)
  }

  return (
    <div className="containerSwitch">
      <Switch
        onColor="#123456"
        offColor="#abc"
        width={60}
        height={20}
        checkedIcon={
          <div className="containerMoon">
            <Moon size={15} color="white" />
          </div>
        }
        uncheckedIcon={
          <div className="containerSun">
            <Sun size={15} color="white" />
          </div>
        }
        onChange={handleChange}
        checked={isDarkTheme}
      />
    </div>
  )
}
